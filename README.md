# Tutorial: Writing a Confluence Theme

This tutorial shows you how to create a Confluence custom theme as a plugin. You'll 
add theme and colour-scheme modules to your plugin to personalize look and feel. 
You can replace your organization's hex codes for the example colors used in this tutorial. 

To read the full tutorial, go to: [Writing a Confluence Theme][1].

## Running locally

To run this app locally, make sure that you have the [Atlassian Plugin SDK][2] installed, and then run:

* atlas-run   -- installs this plugin into the product and starts it on localhost
* atlas-debug -- same as atlas-run, but allows a debugger to attach at port 5005
* atlas-cli   -- after atlas-run or atlas-debug, opens a Maven command line window:
                 - 'pi' reinstalls the plugin into the running product instance
* atlas-help  -- prints description for all commands in the SDK

 [1]: https://developer.atlassian.com/confdev/tutorials/writing-a-confluence-theme
 [2]: https://developer.atlassian.com/docs/getting-started/set-up-the-atlassian-plugin-sdk-and-build-a-project